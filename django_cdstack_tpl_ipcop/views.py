from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_ipcop/django_cdstack_tpl_ipcop"

    template_opts["fail2ban_ipcop_enabled"] = "true"

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
